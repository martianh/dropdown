const loading = document.querySelector('.loading');
const termLoading = document.querySelector('.termloading');
const termCount = document.querySelector('.termcount');
let rhymesMain = [];

// INITIALIZE W ASYNC DATA FETCHING:
document.addEventListener("DOMContentLoaded", function() {
    dbToMain();
});

async function dbToMain() {
    loading.innerText = "Loading...";
    let json = await rhymesFromServerAll();
    console.log(`${json.length} words fetched`);

    let i = 0;
    (function loop() {
        if (i == json.length) {
            termCount.innerText = rhymesMain.length;
            loading.innerText = "Done";
        } else if ( i <= json.length ) {
        // don't upload these back to server as we are downloading them:
        rhymeDictFromRhymes(json[i].term, json[i].rhymes, false);
        termCount.innerText = i;
        i++;
            setTimeout(loop, 10);
        }
    }());
}


// REQUESTS:

async function fetchRhymeBrainJSON(word) {
    termLoading.innerText = word;
    let url = `https://rhymebrain.com/talk`;
    let params = `?function=getRhymes&word=${word}&lang=en&maxResults=100`;
    url = url + params;
    let response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });
    let json = await response.json();
    console.log(`rhymes for ${word} fetched`);
    return json;
}

function rhymesToServer(term, json) {
    let rhymeSet = JSON.stringify({ "term": term, "rhymes": json});
    fetch("/rhymes/add", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: rhymeSet
    });
}

async function rhymesFromServer(term) {
    let request = await fetch(`/rhymes/${term}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    });
    if (request.status === 200) {
        let response = await request.json();
        return response;
    } else {
        return request.status;
    }
}

async function rhymesFromServerAll() {
    let request = await fetch("/rhymes");
    return request.json();
}


// PROCESSING:

async function rhymesFromMainOrFetch(word) {
    let entry = rhymesMain.find(el => el.word === word);
    if (entry === undefined) {
        let dict = await rhymeDictFromRhymes(word);
        return dict;
    } else {
        return entry;
    }
}

async function rhymeDictFromRhymes(word, responseJSON, upload=true) {
    if (responseJSON === undefined && !!word) {
        responseJSON = await fetchRhymeBrainJSON(word);
    }
    let rhymeDict = {
        word: word,
        rhymes: [],
        nearRhymes: [],
    };
    for (let item of responseJSON) {
        if (item.score == 300) { // actual rhyme
            rhymeDict.rhymes.push(item.word);
        } else if (item.score > 200) { // near rhyme
            rhymeDict.nearRhymes.push(item.word);
        }
    }
    // if word not in Main, add its dict:
    if (! (rhymesMain.find(el => el.word === word))) {
        rhymesMain.push(rhymeDict);
    }
    // save any new data to the db:
    if (upload === true) {
        rhymesToServer(word, responseJSON);
    }
    return rhymeDict;
}

function rhymesToHtml (rhymes) {
    let rhymesList = [];
    if (rhymes === undefined) {
        return [];
    } else {
        for (let rhyme of rhymes) {
            rhymesList.push(`<option value="${rhyme}">${rhyme}</option>`);
        }
        return rhymesList;
    }
}

async function makeDropdownForWord(word) {
    let rhymesDict = await rhymesFromMainOrFetch(word);
    let dropdown = "";
    let rhymesHtml = rhymesToHtml(rhymesDict.rhymes);
    let nearRhymesHtml = rhymesToHtml(rhymesDict.nearRhymes);
    let rhymesHtmlString = rhymesHtml.join("");
    let nearRhymesHtmlString = nearRhymesHtml.join("");
    let fullStr = rhymesHtmlString
        + "<option disabled>──────────</option>"
        + nearRhymesHtmlString;

    if (word.length === 0) {
        dropdown = "<br>";
    } else {
        dropdown =
            `<select class="dropdown" name="${word}" id="${word}" onmouseover="dropdownFocus(this)" onwheel="dropdownRotate(this)">
<option selected>${word}</option>`;
    }
    return dropdown.concat(fullStr, `</select>`);
}

async function dropLine(line) {
    // collapse multiple spaces first, so they don't become newlines:
    let lineSplit = line.replace(/ +/g, " ").split(" ");
    let lineArr = [];
    for (let word of lineSplit) {
        let dropdown = await makeDropdownForWord(word);
        lineArr.push(dropdown);
    }
    return lineArr;
}

async function makeDropPoem() {
    const textInput = document.getElementById("PoemInputId").value;
    const dropField = document.querySelector(".dropField");

    let poemArray = await loadDropdownPoem(textInput);
    dropField.innerHTML = poemArray.join("<br>");
    loading.innerText = "Done.";
    termLoading.style = "visibility:hidden";
}

async function loadDropdownPoem(input) {
    const poemLines = input.split(/\n/);
    let poemArray = [];

    for (let line of poemLines) {
        line = line.trim(); // remove trailing whitespace

        if (line === "") { // don't do anything with empty lines
            poemArray.push("");
        } else {
            let lineArray = await dropLine(line);
            let lineDrop = lineArray.join("");
            poemArray.push(lineDrop);
        }
    }
    return poemArray;
}


// BUTTONS:

const subBtn = document.getElementById('submitbtn');

subBtn.addEventListener('click',function() {
    loading.style = "visibility:visible";
    loading.innerText = "Loading...";
    termLoading.style = "visibility:visible";
    makeDropPoem();
});

const copyBtn = document.getElementById('copybtn');

copyBtn.addEventListener('click', function() {
    currentTextAsNew();
});

function getCurrentText() {
    let dropField = document.querySelector(".dropField").innerHTML;
    let drops = document.querySelectorAll(".dropdown");

    for (let drop of drops){
        // replace single dropdown with its selected value:
        let state = drop.value + " ";
        let dropFullHtml = drop.outerHTML;
        dropField = dropField.replace(dropFullHtml, state);

        // handle empty drops:
        // FIXME: improve these awful hacks
        dropField = dropField.replace("  ", " ");

        dropField = dropField.replace("<br><br><br>", "<br><br>");
        dropField = dropField.replace("<br>", "\n");
        // purge terminating whitespace:
        dropField = dropField.replace(" \n", "\n");
    }
    return dropField;
}

function currentTextAsNew() {
    let textStr = getCurrentText();
    document.getElementById('PoemInputId').value = textStr;
}


// MOUSEWHEEL:

function dropdownFocus(x) {
    x.tabIndex = '-1';
    x.focus();
}

function dropdownRotate(element) {
    var scrollDirection = detectMouseWheelDirection();
    // disable window scrolling while we scroll options:
    if (element.addEventListener)
        element.addEventListener("DOMMouseScroll", function(event) {
            event.preventDefault();
        }, false);
    else
        element.attachEvent("mousewheel", function() {
            return false;
        });
    dropdownScrollHandle(element, scrollDirection);
}

function dropdownScrollHandle(element, direction) {
    if ( direction == 'down' ) {
        element.selectedIndex++; // inc our option up
    } else if ( direction == 'up' ) {
        element.selectedIndex--; // inc our option down
    } else {
        element.selectedIndex++; // inc our option up
    }
}

function detectMouseWheelDirection(e) {
    var delta = null,
        direction = false;

    if ( !e ) { // if the event is not provided, we get it from the window object
        e = window.event;
    }
    if ( e.wheelDelta ) { // will work in most cases
        delta = e.wheelDelta / 60;
    } else if ( e.detail ) { // fallback for Firefox
        delta = -e.detail / 2;
    }
    if ( delta !== null ) {
        direction = delta > 0 ? 'up' : 'down';
    }

    return direction;
}

