from flask import Flask, request, jsonify, render_template
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__, static_folder="./", static_url_path="/", template_folder="./")
app.secret_key = b'_5#y2L"F4Q8z\n\asdfasd]/'
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///dropdown.db"

db = SQLAlchemy()
db.init_app(app)

class Rhymeset(db.Model):
    term = db.Column(db.String, primary_key=True)
    rhymes = db.relationship('RhymeBrain', backref='rhymeset')

class RhymeBrain(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    word = db.Column(db.String)
    freq = db.Column(db.Integer)
    score = db.Column(db.Integer)
    flags = db.Column(db.String)
    syllables = db.Column(db.String)
    term_rhyme = db.Column(db.String, db.ForeignKey('rhymeset.term'), unique=False)

with app.app_context():
    db.create_all()

@app.route("/")
def home():
    return render_template("index.html")

@app.route("/rhymes")
def rhymes_list():
    rhymesets = Rhymeset.query.all()
    rjson = []
    for rhymeset in rhymesets:
        json = build_rhymeset_json(rhymeset)
        rjson.append(json)
    return jsonify(rjson)


@app.route("/rhymes/<string:term>")
def get_rhymes_for(term):
    rhymeset = Rhymeset.query.get_or_404(term)
    json = build_rhymeset_json(rhymeset)
    return jsonify(json)


@app.route("/rhymes/add", methods=["POST"])
def rhyme_add():
    if (request.headers.get('Content-Type') == 'application/json'):
        reqjson = request.json # or get_json()
        rhymesjson = reqjson['rhymes']  # list
        rhymeset = Rhymeset(
            term = reqjson['term'],
            rhymes = rhymes_construct(rhymesjson, reqjson['term'])
        )

        # add if not already in db:
        if db.session.query(Rhymeset.term).filter_by(term=reqjson['term']).first() is None:
            db.session.add(rhymeset)
            db.session.commit()
            print("rhymeset added")
        else:
            print("rhymeset not added")
    # else:
        # data = request.data # string
        # do something
    return render_template("index.html")

def rhymes_construct(json, term):
    rhymebrains = []
    for rhyme in json:
        type(rhyme)
        rhymebrains.append(
            RhymeBrain(
                word = rhyme['word'],
                freq = rhyme['freq'],
                score = rhyme['score'],
                flags = rhyme['flags'],
                syllables = rhyme['syllables'],
                term_rhyme = term
            )
        )
    return rhymebrains


def build_rhymeset_json(rhymeset):
    term = rhymeset.term
    rhymebrain = rhymeset.rhymes
    json = { "term": term, "rhymes": build_rhymebrain_json(rhymebrain)}
    return json


def build_rhymebrain_json(rhymebrain):
    json = []
    for rhyme in rhymebrain:
        obj = {"word": rhyme.word,
               "freq": rhyme.freq,
               "score": rhyme.score,
               "flags": rhyme.flags,
               "syllables": rhyme.syllables,
               }
        json.append(obj)
    return json


if __name__ == "__main__":
    app.run(debug=True)
